<?php


namespace App\Controller;


use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleFormType;
use App\Form\CategoryFormType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminBlogController extends AbstractController
{

    /**
     * @Route("/admin/blog/index", name="admin_blog_index")
     * @return Response
     */
    public function indexAction()
    {
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAllArticlesByDateTime();
        return $this->render("Dashboard/blog/index.html.twig", [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("admin/blog/new", name="admin_blog_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $article = new Article();
        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);
        $article->setAuthor($this->getUser());
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($article);
                $em->flush();
                $this->addFlash('success', 'Artykuł został pomyślnie dodany!');

                $this->forward('App\Controller\LogController::logSaveAction', [
                    'content'  => 'Dodał/a artykuł '.'<strong>'.$article->getTitle().'</strong>',
                    'user' => $this->getUser(),
                ]);

                return $this->redirectToRoute('admin_homepage');
            } catch (Exception $e) {

                $this->addFlash('error', 'Błąd! ' . $e->getMessage());
            }
        }
        return $this->render('Dashboard/blog/new.html.twig', [
            'articleForm' => $form->createView(),
        ]);
    }

    /**
     *
     * @Route("/admin/blog/{id}/edit", name="admin_blog_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Article $article
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Article $article)
    {
        $editForm = $this->createForm(ArticleFormType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Pomyślnie zmieniono wpis.');

            return $this->redirectToRoute('admin_blog_index');
        }

        return $this->render('Dashboard/blog/edit.html.twig', array(
            'articles' => $article,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * deleteAction method
     *
     * @Route("/admin/blog/{articleId}/delete", name="admin_blog_delete")
     * @Method("DELETE")
     * @param $articleId
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAction($articleId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $articlePost = $this->getDoctrine()->getRepository(Article::class)->find($articleId);

        if (!($articlePost instanceof Article)) {
            $this->addFlash('error', 'Nie można było usunać tego artykułu. Spróbuj odświeżyć strone!');
        }

        $em->remove($articlePost);
        $em->flush();

        $this->addFlash('success', 'Artykuł został pomyślnie usunięty.');

        return $this->redirectToRoute('admin_blog_index');
    }

    /**
     * @Route("/admin/blog/popular/{id}", name="admin_blog_popular")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function popularAction($id, Request $request)
    {

        $popular = $this->getDoctrine()->getRepository(Article::class)->find($id);

        if ($popular->getPopular() == false || $popular->getPopular() == null) {
            $popular->setPopular(true);
            $this->getDoctrine()->getManager()->flush();
        } else {
            $popular->setPopular(false);
            $this->getDoctrine()->getManager()->flush();
        }
        return new Response("Ustawione!");
    }

    /**
     * @Route("/admin/category/index", name="admin_category_index")
     * @return Response
     */
    public function categoryAction()
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->findAll();
        return $this->render("Dashboard/blog/category.html.twig", [
            'categories' => $category
        ]);
    }

    /**
     * @Route("admin/category/new", name="admin_category_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function newCategory(Request $request, EntityManagerInterface $em)
    {
        $category = new Category();
        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($category);
                $em->flush();
                $this->addFlash('success', 'Kategoria została pomyślnie dodana!');
                return $this->redirectToRoute('admin_category_index');
            } catch (Exception $e) {
                $this->addFlash('error', 'Błąd! ' . $e->getMessage());
            }
        }
        return $this->render('Dashboard/blog/newCategory.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
