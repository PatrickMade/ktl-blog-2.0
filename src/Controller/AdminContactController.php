<?php


namespace App\Controller;


use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminContactController extends AbstractController
{
    /**
     * @Route("/admin/contact/index", name="admin_contact_index")
     */
    public function indexAction()
    {
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->showMailByDesc();

        return $this->render("Dashboard/mail/index.html.twig", [
            'contacts' => $contacts
        ]);
    }

    /**
     * @Route("/admin/contact/view/{id}", name="admin_contact_view")
     * @param $id
     * @return RedirectResponse|Response
     */
    public function viewAction($id)
    {
        $mail = $this->getDoctrine()->getRepository(Contact::class)->find($id);

        if($mail instanceof Contact){
            return $this->render("Dashboard/mail/view.html.twig", [
                'mail' => $mail
            ]);
        }
        return $this->redirectToRoute("admin_contact_index");
    }
}