<?php


namespace App\Controller;


use App\Entity\Avatar;
use App\Entity\Log;
use App\Entity\User;
use App\Form\AvatarFormType;
use App\Form\ChangePasswordFormType;
use App\Form\ProfileFormType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminProfileController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/admin/profile/index", name="admin_profile_index")
     */
    public function indexAction()
    {

        $user = $this->getUser()->getId();
        $userData = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $user]);
        $logs = $this->getDoctrine()->getRepository(Log::class)->findMyLogs($this->getUser());

        return $this->render('Dashboard/profile/index.html.twig', [
            'userData' => $userData,
            'logs' => $logs
        ]);
    }

    /**
     * @Route("admin/profile/edit", name="admin_profile_edit")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @Method({"GET", "POST"})
     * @Template()
     * @throws \Exception
     */
    public function editAction(Request $request, EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $editForm = $this->createForm(ProfileFormType::class, $user);
        $editForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Pomyślnie zmieniono dane użytkownika.');
            return $this->redirectToRoute('admin_profile_index');
        }

        $profile = $this->getUser()->getId();
        $userData = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $profile]);


        return $this->render('Dashboard/profile/edit.html.twig', [
            'userData' => $userData,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("admin/profle/changePassword", name="admin_profile_changepassword")
     * @param Request $request

     * @return Response
     */
    public function passwordAction(Request $request)
    {

        $profile = $this->getUser();
        $changeForm = $this->createForm(ChangePasswordFormType::class);
        $changeForm->handleRequest($request);
        if ($changeForm->isSubmitted() && $changeForm->isValid()) {
            $old = $changeForm->get('oldPassword')->getData();
            if ($this->passwordEncoder->isPasswordValid($profile, $old)) {
                 $newPass = $this->passwordEncoder->encodePassword($profile, $changeForm->get('newPassword')->getData());
                    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $profile->getId()]);
                    $user->setPassword($newPass);
                    $user->setMustChangePassword(false);
                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('success', 'Pomyślnie zmieniono hasło!.');
                    return $this->redirectToRoute('admin_profile_index');
            }
            $this->addFlash('error', 'Nie udało się zmienić hasła, sprawdź dane!.');
        }

        // dane uzytkownika
        $userData = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $profile]);

        return $this->render("Dashboard/profile/changepassword.html.twig", [
            'userData' => $userData,
            'changeForm' => $changeForm->createView()
        ]);
    }


}
