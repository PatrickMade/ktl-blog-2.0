<?php


namespace App\Controller;


use App\Entity\Testimonials;
use App\Form\TestimonialFormType;
use Doctrine\ORM\EntityManagerInterface;
use http\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminTestimonialController extends AbstractController
{

    /**
     * @Route("/admin/testiomonials/index", name="admin_testimonials_index")
     */
    public function indexAction()
    {
        $testimonials = $this->getDoctrine()->getRepository(Testimonials::class)->findAll();

        return $this->render("Dashboard/testimonials/index.html.twig", [
            'testimonials' => $testimonials
        ]);
    }

    /**
     * @Route("admin/testimonial/new", name="admin_testimonial_new")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $testinonial = new Testimonials();

        $form = $this->createForm(TestimonialFormType::class, $testinonial);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($testinonial);
                $em->flush();
                $this->addFlash('success', 'Opinia została pomyślnie dodana!');
                return $this->redirectToRoute('admin_testimonials_index');
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $this->render('Dashboard/testimonials/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/testimonial/delete/{id}", name="admin_testimonial_delete")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $testimonial = $this->getDoctrine()->getRepository(Testimonials::class)->find($id);
        if (!($testimonial instanceof Testimonials)) {
            $this->addFlash('error', 'Nie można było usunać tej opinii. Spróbuj ponownie!');
        }

        $em->remove($testimonial);
        $em->flush();

        $this->addFlash('success', 'Opinia została pomyślnie usunięta.');

        return $this->redirectToRoute('admin_testimonials_index');
    }

    /**
     * @param Request $request
     * @param Testimonials $testimonials
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("admin/testimonial/{id}/edit", name="admin_testimonial_edit")
     */
    public function editAction(Request $request, Testimonials $testimonials)
    {
        $editForm = $this->createForm(TestimonialFormType::class, $testimonials);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Pomyślnie zmieniono opinie.');

            return $this->redirectToRoute('admin_testimonials_index');
        }

        return $this->render('Dashboard/testimonials/edit.html.twig', array(
            'testimonial' => $testimonials,
            'edit_form' => $editForm->createView()
        ));
    }
}