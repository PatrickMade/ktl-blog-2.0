<?php


namespace App\Controller;


use App\Entity\Avatar;
use App\Entity\User;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin/user/list", name="admin_user_list")
     * @return Response
     * @throws Exception
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();


        return $this->render('Dashboard/user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/user/new", name="admin_user_new")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param \Swift_Mailer $mailer
     * @param LoggerInterface $logger
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer, LoggerInterface $logger)
    {
        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $mailTo = $form->get('email')->getData();
            $change = $form->get('password')->getData();
            $user->setAvatar(new Avatar());
            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $form->get('password')->getData()
            ));
            $em->persist($user);
            $em->flush();

            $this->forward('App\Controller\LogController::saveLogAction', [
                'user'  => $this->getUser(),
                'content' => 'test kontent',
            ]);

            $message = (new \Swift_Message('Konto zostało utworzone!'))
                ->setFrom('kontakt@boosttypers.com', 'Boosttypers.com')
                ->setTo($mailTo)
                ->setBody(
                    $this->renderView('Emails/newuser.html.twig', [
                        'newuser' => $user,
                        'password' => $change
                    ]),
                    'text/html'
                );
            $mailer->send($message);

            $this->addFlash('success', 'Użytkownik został pomyślnie dodany!');
            return $this->redirectToRoute('admin_homepage');
        }
        return $this->render('Dashboard/user/new.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/user/delete/{id}", name="admin_user_delete")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if (!($user instanceof User)) {
            $this->addFlash('error', 'Nie udało się usunąć tego użytkownika. Spróbuj ponownie!');
        }

        $em->remove($user);
        $em->flush();

        $this->addFlash('success', 'Użytkownik został usunięty.');

        return $this->redirectToRoute('admin_user_list');
    }

}
