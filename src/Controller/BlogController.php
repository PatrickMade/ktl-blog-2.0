<?php


namespace App\Controller;


use App\Entity\Article;
use App\Entity\Contact;
use App\Form\ContactFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function blogAction()
    {
        $post = $this->getDoctrine()->getRepository(Article::class)->findLastArticle();
        $posts = $this->getDoctrine()->getRepository(Article::class)->showAllArticles();
        $popular = $this->getDoctrine()->getRepository(Article::class)->showPopularArticles();
        unset($posts[0]);


        return $this->render("Front/blog.html.twig", [
            'posts' => $post,
            'allPosts' => $posts,
            'populars' => $popular
        ]);
    }

    /**
     * @Route("/article/{slug}", name="article")
     * @param Request $request
     * @param $slug
     * @param EntityManagerInterface $em
     * @param \Swift_Mailer $mailer
     * @return Response
     * @throws \Exception
     */
    public function articleAction(Request $request, $slug, EntityManagerInterface $em, \Swift_Mailer $mailer)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->findOneBy(['slug' => $slug]);

        $articles = $this->getDoctrine()->getRepository(Article::class)->findLastThreeArticles();
        unset($articles[0]);

        $contact = new Contact();
        $form = $this->createForm(ContactFormType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                $n = 20;
                $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $randomString = "";

                for ($i = 0; $i < $n; $i++) {
                    $index = rand(0, strlen($characters) - 1);
                    $randomString .= $characters[$index];
                }

                $contact
                    ->setShowCode($randomString)
                    ->setChecked(0);


                $em->persist($contact);
                $em->flush();

                $message = (new \Swift_Message('Nowa wiadomość ze strony Boosttypers.com'))
                    ->setFrom('kontakt@boosttypers.com')
                    ->setTo('pawel@boosttypers.com')
                    ->setBody(
                        $this->renderView('Emails/contact.html.twig', [
                            'contact' => $contact,
                            'slug' => $slug
                        ]),
                        'text/html'
                    );
                $mailer->send($message);

                return $this->redirectToRoute('blog');
            } catch (Exception $e) {

            }
        }

        return $this->render('Front/article.html.twig', [
            'article' => $article,
            'lastArticles' => $articles,
            'form' => $form->createView(),
        ]);
    }


}