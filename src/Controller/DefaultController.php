<?php


namespace App\Controller;


use App\Entity\Article;
use App\Entity\Contact;
use App\Entity\Log;
use App\Entity\Logger;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function indexAction()
    {
        $post = $this->getDoctrine()->getRepository(Article::class)->findLastArticle();
        $posts = $this->getDoctrine()->getRepository(Article::class)->showAllArticles();
        $popular = $this->getDoctrine()->getRepository(Article::class)->showPopularArticles();
        unset($posts[0]);


        return $this->render("Default/index.html.twig", [
            'posts' => $post,
            'allPosts' => $posts,
            'populars' => $popular
        ]);
    }

    /**
     * @Route("/admin", name="admin_homepage")
     */
    public function adminIndexAction()
    {
       $logger = $this->getDoctrine()->getRepository(Logger::class)->findLastFiveLoggers();
       $articles = $this->getDoctrine()->getRepository(Article::class)->findLastFiveArticles();
       $logs = $this->getDoctrine()->getRepository(Log::class)->findLastFiveLogs();

        return $this->render('Dashboard/index.html.twig', [
            'logger' => $logger,
            'articles' => $articles,
            'logs' => $logs
        ]);
    }

}
