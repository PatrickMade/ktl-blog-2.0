<?php


namespace App\Controller;


use App\Entity\Log;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use http\Client\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LogController extends AbstractController
{
    private $entityManager;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager, EntityManagerInterface $em)
    {
        $this->entityManager = $entityManager;
        $this->em = $em;
    }

    /**
     * @param string $content
     * @param User $user
     * @return int
     */
    public function logSaveAction(string $content, User $user)
    {
        $log = new Log();
        $log->setTime(new \DateTime())
            ->setUser($user)
            ->setContent($content);
        $this->em->persist($log);
        $this->em->flush();
        return \Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED;
    }
}
