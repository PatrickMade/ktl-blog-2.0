<?php


namespace App\Controller;


use App\Entity\Contact;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class MailController extends AbstractController
{
    /**
     * @Route("/mail/{slug}", name="mailcheck")
     * @param $slug
     * @return RedirectResponse
     */
    public function checkMail($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $mail = $this->getDoctrine()->getRepository(Contact::class)->findOneBy(['showCode' => $slug]);

        if ($mail instanceof Contact){
            $mail->setChecked(1);
            $em->persist($mail);
            $em->flush();
        }
        return $this->redirectToRoute('app_homepage');
    }

}