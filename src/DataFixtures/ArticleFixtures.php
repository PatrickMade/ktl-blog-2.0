<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;

class ArticleFixtures extends BaseFixtures
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(30, 'main_articles', function ($i) use ($manager) {
            $article = new Article();
            $article->setTitle(sprintf('Testowy artykuł numer %d', $i));
            $article->setAuthor(1);
            $article->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Mauris augue neque gravida in fermentum. Cras tincidunt lobortis feugiat vivamus at. Sit amet risus nullam eget felis eget nunc lobortis mattis. Eu nisl nunc mi ipsum faucibus vitae aliquet nec. Est velit egestas dui id ornare. Volutpat odio facilisis mauris sit amet massa. Feugiat nibh sed pulvinar proin. Nisl condimentum id venenatis a. Dignissim convallis aenean et tortor. At auctor urna nunc id cursus metus. Scelerisque eu ultrices vitae auctor eu augue ut. Penatibus et magnis dis parturient montes nascetur ridiculus mus. Lectus mauris ultrices eros in cursus turpis massa tincidunt. In tellus integer feugiat scelerisque varius. At consectetur lorem donec massa sapien faucibus. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Arcu risus quis varius quam quisque id diam. Enim sit amet venenatis urna cursus eget nunc scelerisque.
                                        Scelerisque in dictum non consectetur a erat nam at lectus. A arcu cursus vitae congue mauris. Cursus metus aliquam eleifend mi in nulla posuere sollicitudin aliquam. Convallis posuere morbi leo urna molestie at elementum eu. Sollicitudin ac orci phasellus egestas. Eu scelerisque felis imperdiet proin. Posuere ac ut consequat semper viverra nam. Mi in nulla posuere sollicitudin aliquam ultrices sagittis orci a. Felis donec et odio pellentesque diam. Pulvinar elementum integer enim neque volutpat ac tincidunt vitae. Pharetra convallis posuere morbi leo. Vivamus arcu felis bibendum ut tristique et egestas quis ipsum. Proin libero nunc consequat interdum varius sit amet. Augue mauris augue neque gravida in fermentum et sollicitudin. Aliquam eleifend mi in nulla posuere sollicitudin aliquam. Euismod quis viverra nibh cras. Etiam tempor orci eu lobortis elementum nibh tellus molestie. Nunc eget lorem dolor sed viverra ipsum nunc aliquet. Egestas pretium aenean pharetra magna ac placerat.
                                        Pharetra magna ac placerat vestibulum lectus mauris ultrices. Et netus et malesuada fames ac turpis. Cras fermentum odio eu feugiat pretium nibh ipsum consequat. Non arcu risus quis varius. Erat velit scelerisque in dictum non consectetur a erat nam. Scelerisque purus semper eget duis at. Cursus risus at ultrices mi tempus imperdiet nulla malesuada. In ante metus dictum at tempor commodo. Eu ultrices vitae auctor eu augue. Risus ultricies tristique nulla aliquet enim tortor at auctor. Egestas dui id ornare arcu odio ut sem nulla pharetra. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Mauris a diam maecenas sed enim ut sem. Curabitur gravida arcu ac tortor. Blandit cursus risus at ultrices. Augue ut lectus arcu bibendum at varius vel pharetra.
                                        Molestie ac feugiat sed lectus. In vitae turpis massa sed elementum tempus egestas. Sed arcu non odio euismod lacinia at quis risus. Quis blandit turpis cursus in hac. Auctor elit sed vulputate mi sit amet mauris. Id interdum velit laoreet id donec ultrices tincidunt arcu. In iaculis nunc sed augue lacus viverra. Mauris commodo quis imperdiet massa tincidunt nunc. Vitae tempus quam pellentesque nec nam aliquam sem. Et malesuada fames ac turpis egestas. Justo donec enim diam vulputate. Quisque id diam vel quam elementum pulvinar etiam non. Eu scelerisque felis imperdiet proin fermentum leo. Ut venenatis tellus in metus vulputate eu scelerisque felis. Augue interdum velit euismod in pellentesque massa placerat duis. Vestibulum mattis ullamcorper velit sed.
                                        Rhoncus mattis rhoncus urna neque viverra justo nec. Sit amet consectetur adipiscing elit pellentesque. Ut etiam sit amet nisl purus in. Suspendisse ultrices gravida dictum fusce ut placerat orci nulla. A scelerisque purus semper eget duis at tellus at. Fames ac turpis egestas integer eget aliquet nibh praesent tristique. Nunc sed augue lacus viverra. Vitae ultricies leo integer malesuada nunc vel risus. Tellus elementum sagittis vitae et leo. Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Ut aliquam purus sit amet luctus venenatis lectus. Et netus et malesuada fames. Amet facilisis magna etiam tempor. Vitae congue eu consequat ac. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Nunc aliquet bibendum enim facilisis gravida neque. Condimentum lacinia quis vel eros donec. Lectus vestibulum mattis ullamcorper velit sed. Nec feugiat in fermentum posuere urna nec. Lorem dolor sed viverra ipsum nunc aliquet bibendum enim facilisis.');
            $article->setShortText('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Mauris augue neque gravida in fermentum. Cras tincidunt lobortis feugiat vivamus at. Sit amet risus nullam eget felis eget nunc lobortis mattis.');
            $article->setActive(1);
            $article->prePersist();
            $manager->persist($article);

            return $article;
        });
    }
}
