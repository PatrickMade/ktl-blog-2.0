<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @property  faker
 */
class UserFixtures extends BaseFixtures
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(1, 'main_users', function($i) use ($manager) {
            $user = new User();
            $user->setEmail(sprintf('user%d@ktl.pl', $i));
            $user->setUsername(sprintf('user%d', $i));
            $user->setName('Albert Einstein');


            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                '12345'
            ));

            $manager->persist($user);

            return $user;
        });

        $this->createMany(1, 'admin_users', function($i) {
            $user = new User();
            $user->setEmail(sprintf('admin%d@ktl.pl', $i));
            $user->setUsername(sprintf('admin%d', $i));
            $user->setName($this->faker->firstName);
            $user->setRoles(['ROLE_ADMIN']);

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                '12345678'
            ));

            return $user;
        });

        $manager->flush();
    }
}
