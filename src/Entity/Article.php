<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $shortText;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;


    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $author;


    /**
     * pictureFile field
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Assert\Image(
     *     minHeight="100",
     *     minHeightMessage="Wysokość przesłanego zdjęcia ({{ height }}px) musi być większa niż {{ min_height }}px.",
     *     maxHeight="2048",
     *     maxHeightMessage="Wysokość przesłanego zdjęcia ({{ height }}px) nie może przekraczać {{ max_height }}px.",
     *     minWidth="100",
     *     minWidthMessage="Szerokość przesłanego zdjęcia ({{ width }}px) musi być większa niż {{ min_width }}px.",
     *     maxWidth="2048",
     *     maxWidthMessage="Szerokość przesłanego zdjęcia ({{ width }}px) nie może przekraczać {{ max_width }}px."
     * )
     *
     * @Vich\UploadableField(
     *     mapping="post_image", fileNameProperty="pictureName", size="pictureSize"
     * )
     * @var File
     */
    private $pictureFile;

    /**
     * pictureName field
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $pictureName;

    /**
     * pictureSize field
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $pictureSize;

    /**
     * @ORM\Column(type="integer")
     */
    private $readTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $popular;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Seo", cascade={"persist", "remove"})
     */
    private $seo;

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }


    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getShortText()
    {
        return $this->shortText;
    }

    /**
     * @param mixed $shortText
     * @return Article
     */
    public function setShortText($shortText): self
    {
        $this->shortText = $shortText;
        return $this;
    }

    /**
     * @param mixed $active
     * @return Article
     */
    public function setActive($active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * setPictureFile method
     *
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Article
     * @throws \Exception
     */
    public function setPictureFile(File $image = null): Article
    {
        $this->pictureFile = $image;

        if ($image)
        {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * getPictureFile method
     *
     * @return File|null
     */
    public function getPictureFile(): ?File
    {
        return $this->pictureFile;
    }

    /**
     * setPictureName method
     *
     * @param string $imageName
     *
     * @return Article
     */
    public function setPictureName($imageName): Article
    {
        $this->pictureName = $imageName;

        return $this;
    }

    /**
     * getPictureName method
     *
     * @return string|null
     */
    public function getPictureName(): ?string
    {
        return $this->pictureName;
    }

    /**
     * setPictureSize method
     *
     * @param integer $imageSize
     *
     * @return Article
     */
    public function setPictureSize($imageSize): Article
    {
        $this->pictureSize = $imageSize;

        return $this;
    }

    /**
     * getPictureSize method
     *
     * @return integer|null
     */
    public function getPictureSize(): ?int
    {
        return $this->pictureSize;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Article
     */
    public function setAuthor($author): self
    {
        $this->author = $author;
        return $this;
    }

    public function getReadTime(): ?string
    {
        return $this->readTime;
    }

    public function setReadTime(int $readTime): self
    {
        $this->readTime = $readTime;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPopular()
    {
        return $this->popular;
    }

    /**
     * @param mixed $popular
     * @return Article
     */
    public function setPopular($popular): self
    {
        $this->popular = $popular;
        return $this;
    }

    public function getSeo(): ?Seo
    {
        return $this->seo;
    }

    public function setSeo(?Seo $seo): self
    {
        $this->seo = $seo;

        return $this;
    }

}
