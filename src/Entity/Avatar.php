<?php

namespace App\Entity;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvatarRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class Avatar implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * pictureFile field
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Assert\Image(
     *     minHeight="100",
     *     minHeightMessage="Wysokość przesłanego zdjęcia ({{ height }}px) musi być większa niż {{ min_height }}px.",
     *     maxHeight="2048",
     *     maxHeightMessage="Wysokość przesłanego zdjęcia ({{ height }}px) nie może przekraczać {{ max_height }}px.",
     *     minWidth="100",
     *     minWidthMessage="Szerokość przesłanego zdjęcia ({{ width }}px) musi być większa niż {{ min_width }}px.",
     *     maxWidth="2048",
     *     maxWidthMessage="Szerokość przesłanego zdjęcia ({{ width }}px) nie może przekraczać {{ max_width }}px."
     * )
     *
     * @Vich\UploadableField(
     *     mapping="avatar", fileNameProperty="pictureName", size="pictureSize"
     * )
     * @var File
     */
    private $pictureFile;

    /**
     * pictureName field
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $pictureName;

    /**
     * pictureSize field
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $pictureSize;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $changeString;

    /**
     * setPictureFile method
     *
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile $image
     *
     * @return Avatar
     * @throws Exception
     */
    public function setPictureFile(File $image = null): self
    {
        $this->pictureFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }

    /**
     * getPictureFile method
     *
     * @return File|null
     */
    public function getPictureFile(): ?File
    {
        return $this->pictureFile;
    }

    /**
     * setPictureName method
     *
     * @param string $imageName
     *
     * @return Avatar
     */
    public function setPictureName($imageName): self
    {
        $this->pictureName = $imageName;

        return $this;
    }

    /**
     * getPictureName method
     *
     * @return string|null
     */
    public function getPictureName(): ?string
    {
        return $this->pictureName;
    }

    /**
     * setPictureSize method
     *
     * @param integer $imageSize
     *
     * @return Avatar
     */
    public function setPictureSize($imageSize): self
    {
        $this->pictureSize = $imageSize;

        return $this;
    }

    /**
     * getPictureSize method
     *
     * @return integer|null
     */
    public function getPictureSize(): ?int
    {
        return $this->pictureSize;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getChangeString()
    {
        return $this->changeString;
    }

    /**
     * @param mixed $changeString
     * @return Avatar
     */
    public function setChangeString($changeString): self
    {
        $this->changeString = $changeString;
        return $this;
    }


    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->pictureName,
        ));
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->pictureName,
            ) = unserialize($serialized, array('allowed_classes' => false));
    }
}
