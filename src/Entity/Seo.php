<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seo
 * @ORM\Table(name="seo")
 * @ORM\Entity(repositoryClass="App\Repository\SeoRepository")
 */
class Seo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $robots;

    /**
     * @var array
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $socialTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $socialDesc;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $socialImage;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="seos")
     * @ORM\JoinColumn(name="article", referencedColumnName="id", onDelete="CASCADE")
     */
    private $article;

    /**
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isTitleParent;

    /**
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isTitleMain;

    /**
     *
     * @var bool
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $canonical;

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Seo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Seo
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set robots
     *
     * @param string $robots
     *
     * @return Seo
     */
    public function setRobots($robots)
    {
        $this->robots = $robots;

        return $this;
    }

    /**
     * Get robots
     *
     * @return string
     */
    public function getRobots()
    {
        return $this->robots;
    }

    /**
     * Set keywords
     *
     * @param array $keywords
     *
     * @return Seo
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return array
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Seo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set socialTitle
     *
     * @param string $socialTitle
     *
     * @return Seo
     */
    public function setSocialTitle($socialTitle)
    {
        $this->socialTitle = $socialTitle;

        return $this;
    }

    /**
     * Get socialTitle
     *
     * @return string
     */
    public function getSocialTitle()
    {
        return $this->socialTitle;
    }

    /**
     * Set socialDesc
     *
     * @param string $socialDesc
     *
     * @return Seo
     */
    public function setSocialDesc($socialDesc)
    {
        $this->socialDesc = $socialDesc;

        return $this;
    }

    /**
     * Get socialDesc
     *
     * @return string
     */
    public function getSocialDesc()
    {
        return $this->socialDesc;
    }

    /**
     * Set socialImage
     *
     * @param string $socialImage
     *
     * @return Seo
     */
    public function setSocialImage($socialImage)
    {
        $this->socialImage = $socialImage;

        return $this;
    }

    /**
     * Get socialImage
     *
     * @return string
     */
    public function getSocialImage()
    {
        return $this->socialImage;
    }


    /**
     * Set isTitleParent
     *
     * @param boolean $isTitleParent
     *
     * @return Seo
     */
    public function setIsTitleParent($isTitleParent)
    {
        $this->isTitleParent = $isTitleParent;

        return $this;
    }

    /**
     * Get isTitleParent
     *
     * @return boolean
     */
    public function getIsTitleParent()
    {
        return $this->isTitleParent;
    }

    /**
     * Set isTitleMain
     *
     * @param boolean $isTitleMain
     *
     * @return Seo
     */
    public function setIsTitleMain($isTitleMain)
    {
        $this->isTitleMain = $isTitleMain;

        return $this;
    }

    /**
     * Get isTitleMain
     *
     * @return boolean
     */
    public function getIsTitleMain()
    {
        return $this->isTitleMain;
    }

    /**
     * Set canonical
     *
     * @param boolean $canonical
     *
     * @return Seo
     */
    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;

        return $this;
    }

    /**
     * Get canonical
     *
     * @return boolean
     */
    public function getCanonical()
    {
        return $this->canonical;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     * @return $this
     */
    public function setArticle($article): self
    {
        $this->article = $article;
        return $this;
    }

}
