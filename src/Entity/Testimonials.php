<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestimonialsRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class Testimonials
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * pictureFile field
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Assert\Image(
     *     minHeight="20",
     *     minHeightMessage="Wysokość przesłanego zdjęcia ({{ height }}px) musi być większa niż {{ min_height }}px.",
     *     maxHeight="2048",
     *     maxHeightMessage="Wysokość przesłanego zdjęcia ({{ height }}px) nie może przekraczać {{ max_height }}px.",
     *     minWidth="20",
     *     minWidthMessage="Szerokość przesłanego zdjęcia ({{ width }}px) musi być większa niż {{ min_width }}px.",
     *     maxWidth="2048",
     *     maxWidthMessage="Szerokość przesłanego zdjęcia ({{ width }}px) nie może przekraczać {{ max_width }}px."
     * )
     *
     * @Vich\UploadableField(
     *     mapping="post_image", fileNameProperty="pictureName", size="pictureSize"
     * )
     * @var File
     */
    private $pictureFile;

    /**
     * pictureName field
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $pictureName;

    /**
     * pictureSize field
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $pictureSize;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return Testimonials
     */
    public function setActive($active): self
    {
        $this->active = $active;
        return $this;
    }

    /**
     * setPictureFile method
     *
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile $image
     *
     * @return Testimonials
     * @throws \Exception
     */
    public function setPictureFile(File $image = null): Testimonials
    {
        $this->pictureFile = $image;

        if ($image)
        {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }

    /**
     * getPictureFile method
     *
     * @return File|null
     */
    public function getPictureFile(): ?File
    {
        return $this->pictureFile;
    }

    /**
     * setPictureName method
     *
     * @param string $imageName
     *
     * @return Testimonials
     */
    public function setPictureName($imageName): Testimonials
    {
        $this->pictureName = $imageName;

        return $this;
    }

    /**
     * getPictureName method
     *
     * @return string|null
     */
    public function getPictureName(): ?string
    {
        return $this->pictureName;
    }

    /**
     * setPictureSize method
     *
     * @param integer $imageSize
     *
     * @return Testimonials
     */
    public function setPictureSize($imageSize): Testimonials
    {
        $this->pictureSize = $imageSize;

        return $this;
    }

    /**
     * getPictureSize method
     *
     * @return integer|null
     */
    public function getPictureSize(): ?int
    {
        return $this->pictureSize;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
