<?php


namespace App\Form;


use App\Entity\Article;
use App\Entity\Seo;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Tytuł',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Krótki tekst',
                'attr' => [
                    'class' => 'summernote',
                ]
            ])
            ->add('shortText', TextareaType::class, [
                'label' => 'Krótki tekst',
                'attr' => [
                    'class' => 'summernote',
                ]
            ])
            ->add('pictureFile', VichImageType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dropzone-msg dz-message needsclick'
                ],
                'allow_delete' => true,
                'delete_label' => 'Usunąć?',
                'download_label' => 'Pełny rozmiar',
                'download_uri' => true,
                'image_uri' => true,
                'imagine_pattern' => 'post_thumbnail',
                'label' => 'Obraz'
            ])
            ->add('readTime', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Liczba minut'
                ],
                'required' => false

            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'data-switch' => 'true',
                    'data-on-text' => 'aktywny',
                    'data-off-text' => 'Nieaktywny',
                    'data-on-color' => 'success'
                ],
                'required' => false,

            ])
            ->add('seo', SeoType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Article::class,
            'label' => false
        ));
    }
}
