<?php


namespace App\Form;


use App\Entity\Candidates;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CandidatesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('mail', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('note', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Info..'
                ],
                'required' => false
            ])
            ->add('type', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-control kt-select2',
                    'name' => '_kt_select2_1'
                ],
                'choices'  => [
                    'Freelancer' => 'Freelancer',
                    'Pełny Etat' => 'Pełny etat'
                ]
            ])
            ->add('rating', ChoiceType::class, [
                'attr' => [
                  'class' => 'form-control kt-select2',
                  'name' => '_kt_select2_1'
                ],
                'choices'  => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ]
            ])
            ->add('gitLink', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'https://github.com/'
                ]
            ])
            ->add('fileFile', VichFileType::class, [
                'attr' => [
                    'class' => 'form-control'
                    ]
            ])
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Candidates::class,
            'label' => false
        ));
    }
}