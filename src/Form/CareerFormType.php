<?php


namespace App\Form;


use App\Entity\Career;
use App\Entity\Technology;
use App\Entity\Techstack;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CareerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Tytuł',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Opis',
                'attr' => [
                    'class' => 'ckeditor',
                ]
            ])
            ->add('rate', TextType::class, [
                'label' => 'Widełki (tekst)',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('formOfCooperation', ChoiceType::class, [
                'multiple' => true,
                'label' => 'Login',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'B2B' => 'B2B',
                    'B2C' => 'B2C',
                    'UoP' => 'UoP',
                    'UZ' => 'UZ',
                    'UoD' => 'UoD'
                ]
            ])
            ->add('level', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-control kt-select2',
                    'name' => '_kt_select2_1'
                ],
                'choices'  => [
                    'JNR' => 'Junior',
                    'MID' => 'MID',
                    'SNR' => 'Senior',
                ]
            ])
            ->add('niceToHave', TextareaType::class, [
                'label' => 'Wymagania',
                'attr' => [
                    'class' => 'ckeditor',
                ]
            ])
            ->add('ifYouHave', TextareaType::class, [
                'label' => 'Mile widziane',
                'attr' => [
                    'class' => 'ckeditor',
                ]
            ])
            ->add('responsibilities', TextareaType::class, [
                'label' => 'Metodologia pracy',
                'attr' => [
                    'class' => 'ckeditor',
                ]
            ])
            ->add('type', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('technology', EntityType::class, [
                'class' => Technology::class,
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('techstack', EntityType::class, [
                'class' => Techstack::class,
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'checked' => 'true'
                ],
                'required' => false,
            ])
            ->add('pictureFile', VichImageType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dropzone-msg dz-message needsclick'
                ],
                'allow_delete' => true,
                'delete_label' => 'Usunąć?',
                'download_label' => 'Pełny rozmiar',
                'download_uri' => true,
                'image_uri' => true,
                'imagine_pattern' => 'post_thumbnail',
                'label' => 'Obraz'
            ])
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Career::class,
            'label' => false
        ));
    }
}