<?php


namespace App\Form;


use App\Entity\CaseCategory;
use App\Entity\Casestudy;
use App\Entity\Technology;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CasestudyFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Tytuł',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('shortText', TextareaType::class, [
                'label' => 'Krótki tekst',
                'attr' => [
                    'class' => 'ckeditor',
                ]
            ])
            ->add('pictureFile', VichImageType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dropzone-msg dz-message needsclick'
                ],
                'allow_delete' => true,
                'delete_label' => 'Usunąć?',
                'download_label' => 'Pełny rozmiar',
                'download_uri' => true,
                'image_uri' => true,
                'imagine_pattern' => 'post_thumbnail',
                'label' => 'Obraz'
            ])
            ->add('technology', EntityType::class, [
                'class' => Technology::class,
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('category', EntityType::class, [
                'class' => CaseCategory::class,
                'multiple' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'data-switch' => 'true',
                    'data-on-text' => 'aktywny',
                    'data-off-text' => 'Nieaktywny',
                    'data-on-color' => 'success'
                ],
                'required' => false,

            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Casestudy::class,
            'label' => false
        ));
    }
}