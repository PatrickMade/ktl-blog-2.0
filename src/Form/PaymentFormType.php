<?php


namespace App\Form;


use App\Entity\Payment;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\DBAL\Types\JsonType;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Json;

class PaymentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('service', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('netAmount', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('grossAmount', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
            ->add('cyclic', CheckboxType::class, [
                'label' => 'Login',
                'attr' => [
                    'class' => ''
                ],
                'required' => false
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'checked' => 'true'
                ],
                'required' => false,
            ])
            ->add('oneDayNotification', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'data-switch' => 'true',
                    'data-on-text' => 'aktywny',
                    'data-off-text' => 'Nieaktywny',
                    'data-on-color' => 'info'
                ],
                'required' => false,
            ])
            ->add('threeDaysNotification', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'data-switch' => 'true',
                    'data-on-text' => 'aktywny',
                    'data-off-text' => 'Nieaktywny',
                    'data-on-color' => 'info'
                ],
                'required' => false,
            ])
            ->add('sevenDaysNotification', CheckboxType::class, [
                'label' => 'Aktywny?',
                'attr' => [
                    'data-switch' => 'true',
                    'data-on-text' => 'aktywny',
                    'data-off-text' => 'Nieaktywny',
                    'data-on-color' => 'info'
                ],
                'required' => false,

            ])
            ->add('foreignPayment', CheckboxType::class, [
                'attr' => [
                    'class' => 'checkvalue'
                ],
                'required' => false
            ])
            ->add('delayTime', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-control delay'
                ],
                'choices' => [
                    '7 dni' => '+7 day',
                    '14 dni' => '+14 day',
                    'miesiąc' => '+1 month',
                    '2 miesiące' => '+2 month',
                    '3 miesiące' => '+3 month',
                    '6 miesięcy' => '+6 month',
                    'rok' => '+1 year'
                ],
                'required' => false
            ])
            ->add('isAutomatic', CheckboxType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Payment::class,
            'label' => false
        ));
    }
}