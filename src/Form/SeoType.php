<?php

namespace App\Form;

use App\Entity\Seo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ))
            ->add('slug', TextType::class, array(
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ))
            ->add('robots', ChoiceType::class, array('label' => false, 'required' => false,
                'choices' => array(
                    'INDEX,FOLLOW' => 'INDEX,FOLLOW',
                    'NOINDEX, FOLLOW' => 'NOINDEX, FOLLOW',
                    'INDEX, NOFOLLOW' => 'INDEX, NOFOLLOW',
                    'NOINDEX, NOFOLLOW' => 'NOINDEX, NOFOLLOW',
                ),
                'attr' => [
                    'class' => 'form-control kt-select2',
                    'name' => '_kt_select2_1'
                ],
            ))

            ->add('description', TextType::class, array(
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ))
            ->add('canonical', TextType::class, array(
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Seo::class,
            'allow_extra_fields' => false,
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
