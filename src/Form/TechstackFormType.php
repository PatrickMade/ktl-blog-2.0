<?php


namespace App\Form;


use App\Entity\Techstack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class TechstackFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Tytuł',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('rate', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-control kt-select2',
                    'name' => '_kt_select2_1'
                ],
                'choices'  => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ]
            ])
            ->add('pictureFile', VichImageType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dropzone-msg dz-message needsclick'
                ],
                'allow_delete' => true,
                'delete_label' => 'Usunąć?',
                'download_label' => 'Pełny rozmiar',
                'download_uri' => true,
                'image_uri' => true,
                'imagine_pattern' => 'post_thumbnail',
                'label' => 'Obraz'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Techstack::class,
            'label' => false
        ));
    }
}