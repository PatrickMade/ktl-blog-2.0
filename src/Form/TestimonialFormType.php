<?php


namespace App\Form;


use App\Entity\Testimonials;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class TestimonialFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('company', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('pictureFile', VichImageType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'dropzone-msg dz-message needsclick'
                ],
                'allow_delete' => true,
                'delete_label' => 'Usunąć?',
                'download_label' => 'Pełny rozmiar',
                'download_uri' => false,
                'image_uri' => false,
                'imagine_pattern' => 'post_thumbnail',
                'label' => 'Avatar'
            ])
            ->add('active', CheckboxType::class, [
                'attr' => [
                    'checked' => 'true'
                ],
                'required' => false,
            ]);
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Testimonials::class,
            'label' => false
        ));
    }
}