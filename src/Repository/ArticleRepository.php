<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class ArticleRepository extends EntityRepository
{

    public function showPopularArticles()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.active = 1')
            ->andWhere('p.popular = true')
            ->orderBy('p.createdAt', 'desc');

        return $qb->getQuery()->getResult();
    }

    public function showAllArticles()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.active = 1')
            ->orderBy('p.createdAt', 'desc');

        return $qb->getQuery()->getResult();
    }


    public function findLastThreeArticles()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.active = 1')
            ->orderBy('p.id', 'desc')
            ->setMaxResults(4);

        return $qb->getQuery()->getResult();
    }

    public function findLastArticle()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.active = 1')
            ->orderBy('p.id', 'desc')
            ->setMaxResults(1);

        return $qb->getQuery()->getResult();
    }

    public function findLastFiveArticles()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.active = 1')
            ->orderBy('p.createdAt', 'desc')
            ->setMaxResults(5);

        return $qb->getQuery()->getResult();
    }

    public function findAllArticlesByDateTime()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->orderBy('p.createdAt', 'desc');

        return $qb->getQuery()->getResult();
    }

    public function getCountOfArticles(){
        $qb = $this->createQueryBuilder('i');
        try {
            return $qb->select('count(i.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            //exception
        }
    }
}
