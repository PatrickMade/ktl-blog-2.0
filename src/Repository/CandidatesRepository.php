<?php

namespace App\Repository;

use App\Entity\Candidates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Candidates|null find($id, $lockMode = null, $lockVersion = null)
 * @method Candidates|null findOneBy(array $criteria, array $orderBy = null)
 * @method Candidates[]    findAll()
 * @method Candidates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Candidates::class);
    }

    public function getFullWorkTime()
    {
        $qb = $this->createQueryBuilder('l');
        return
            $qb->select('l')
                ->where("l.type like 'Pełny etat'")
                ->getQuery()
                ->getResult()
            ;
    }


    public function getFreelancer()
    {
        $qb = $this->createQueryBuilder('l');
        return
            $qb->select('l')
                ->where("l.type like 'Freelancer'")
                ->getQuery()
                ->getResult()
            ;
    }
}
