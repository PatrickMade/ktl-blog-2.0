<?php

namespace App\Repository;

use App\Entity\Career;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method Career|null find($id, $lockMode = null, $lockVersion = null)
 * @method Career|null findOneBy(array $criteria, array $orderBy = null)
 * @method Career[]    findAll()
 * @method Career[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CareerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Career::class);
    }

    public function getCountOfCareer(){
        $qb = $this->createQueryBuilder('i');
        try {
            return $qb->select('count(i.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function showActive()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.active = 1')
            ->orderBy('p.id', 'desc');

        return $qb->getQuery()->getResult();
    }
}
