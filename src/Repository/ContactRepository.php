<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;


class ContactRepository extends EntityRepository
{
    public function showMailByDesc()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->orderBy('p.createdAt', 'desc');

        return $qb->getQuery()->getResult();
    }

    public function countIt()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id) as count')
            ->where('d.checked = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

}
