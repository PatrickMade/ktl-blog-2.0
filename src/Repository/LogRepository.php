<?php

namespace App\Repository;

use App\Entity\Log;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Log|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log[]    findAll()
 * @method Log[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Log::class);
    }

    public function findLastFiveLogs()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->orderBy('p.time', 'desc')
            ->setMaxResults(5);

        return $qb->getQuery()->getResult();
    }

    public function findMyLogs(User $user)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.user = :user')
            ->orderBy('p.time', 'desc')
            ->setParameter('user', $user->getId())
            ->setMaxResults(5);

        return $qb->getQuery()->getResult();
    }
}
