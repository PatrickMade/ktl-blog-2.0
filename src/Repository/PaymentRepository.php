<?php

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    public function showOctober()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'desc')
            ->where('p.archive = 0 OR p.archive is null AND month(p.paymentDate) = 10')
            ->getQuery()
            ->getResult();
    }
    public function showNovember()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'desc')
            ->where('p.archive = 0 OR p.archive is null AND month(p.paymentDate) = 11')
            ->getQuery()
            ->getResult();
    }
    public function showDecember()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'desc')
            ->where('p.archive = 0 OR p.archive is null AND month(p.paymentDate) = 12')
            ->getQuery()
            ->getResult();
    }

    public function showJanuary()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'desc')
            ->where('p.archive = 0 OR p.archive is null AND month(p.paymentDate) = 1')
            ->getQuery()
            ->getResult();
    }

    public function showFebruary()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'desc')
            ->where('p.archive = 0 OR p.archive is null AND month(p.paymentDate) = 2')
            ->getQuery()
            ->getResult();
    }

    public function showClosedPayments()
    {
        return $this->createQueryBuilder('p')
            ->where("p.paid like '1'")
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function showCountPayments()
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function showPaymentsChronological()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.paymentDate > CURRENT_DATE()')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'asc')
            ->getQuery()
            ->setMaxResults(8)
            ->getResult();
    }

    public function showArchivePayments()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.paymentDate > CURRENT_DATE()')
            ->andWhere('p.archive = true')
            ->orderBy('CURRENT_DATE(), p.paymentDate', 'asc')
            ->getQuery()
            ->getResult();
    }
}
