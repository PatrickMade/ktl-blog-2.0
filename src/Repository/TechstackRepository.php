<?php

namespace App\Repository;

use App\Entity\Techstack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Techstack|null find($id, $lockMode = null, $lockVersion = null)
 * @method Techstack|null findOneBy(array $criteria, array $orderBy = null)
 * @method Techstack[]    findAll()
 * @method Techstack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechstackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Techstack::class);
    }

    // /**
    //  * @return Techstack[] Returns an array of Techstack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Techstack
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
