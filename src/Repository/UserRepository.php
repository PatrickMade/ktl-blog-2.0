<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Flex\Unpack\Result;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }


    public function getCountOfUsers()
    {
        $qb = $this->createQueryBuilder('i');
        try {
            return $qb->select('count(i.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            //exception
        }
    }

    public function getUsersListLogin()
    {
        $qb = $this->createQueryBuilder('u');

        return
            $qb->select('u')
            ->where('u.lastLoginAt is not null')
            ->orderBy('u.lastLoginAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
            ;

    }

    public function getUsersLastLogin()
    {
        $qb = $this->createQueryBuilder('l');

        return
            $qb->select('l.lastLoginAt')
                ->where('u.lastLoginAt is not null')
                ->getQuery()
                ->getResult()
            ;

    }

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
